# vue_webcamera

#### 介绍
vue_webcamera 为 eloam SDK：webCamera 的 vue demo，现在很多项目都是采用的vue编写的前端了，但是部分客户在发送http请求时，出现一些意外，所以，我们编写了一个vue的demo，供大家借鉴。

目前集成了webCamera所有的接口，后期webCamera如有更新，此demo也会做到及时更新。

vue工程使用的 vue/cli v4.5.13, vue3编写


！如果您并非开发人员，不懂代码，可以使用 dist 文件夹中的 index.html 来进行测试。

